Mercudi Version 0.3.3
Author: Akhlak Mahmood

Mercudi - Simple, highly configurable, multithreaded web crawler.


How to use
======================================================================
To implement the crawler, create a new class which is a child of
Mercudi.RulesInterface.IRules.

Example MySiteCrawler.py:

import os
from Mercudi.RulesInterface import IRules
import Mercudi.Global as Gl
from Mercudi import Mercudi3

class MyRules(IRules):
    StartUrls = ["http://my.sitetocrawl.com/start/page1", "http://my.sitetocrawl.com/start/page2"]
    CrawlerDirectory = os.path.join(os.getcwd(), "MySite")
    DownloadsDirectory = os.path.join(CrawlerDirectory, "Downloads")
    nNetThreads = 2

    def __init__(self):
        super(IRules, self).__init__()

    def ShouldVisit(self, url):
        return False

    def ShouldDownload(self, url):
        return True

    def DownloadImgSrc(self):
        return True

Mercudi3.Run(MyRules, False)

This is a minimal working crawler which will download all
img src from the given start url into the folder MySiteDownloads.
You can give the list of urls to start with in the StartUrls.
It is good to have same no of nNetThreads, in this case 2.

The optional second argument of Run() is a bool directing whether to load
the previous saved state if any. The default value is False.

TIPS:
    1. Use the debug output for matched links by True or False.

    2. First crawl the first pages by setting nDownloadsThreads = 0
    Then set nNetThreads = nParseThreads = 0 and LoadPrevState = True
    To download the previously found files.

    3. Some files won't be reached due to HTTP Errors. Rerun the downloader
    to retry the non visited files.

    4. To kill the thread just copy the killer in CWD. See below for details.

    5. ProcessLink() is helpful to extract the redirection links by url=

    6. See the already done projects for best idea.

About your Rule Class
=============================================================================
It must inherit the Mercudi.RulesInterface

Only required methods and properties should be overridden

StartUrls must be set with your desired urls to start crawling
with otherwise it will crawl the default python.org

By default Mercudi will crawl every link it finds. Its your
resposibility to direct it by ShouldVisit() method.

By default Mercudi will not download any file. Its upto you to
tell it to download using ShouldDownload() method.


Call Hierarchy of the Mercudi Parser
==============================================================================
Below is the call hierarchy of each html. All Rules are called
from here and only it determines how your site will be crawled.
For the best result of your crawling see the calling process below
and make changes to your Rule Class appropiately.

    For each html in the htmlQ:
        ... extract link from html using given regex
        For each item:
            ProcessAchorRegexResult(item)
            urljoin(item) if not startwith http://
            isValidUrl(item)?
                FormatUrl(item)
                isDownloadableUrl(item)?
                    ... Add item to Dloads
                else:
                    ... Add item to Urls
        DownloadImgSrc?
            ... Get all img srcs from the html using the given regex
            For each src:
                ProcessSrcRegexResult(src)
                urljoin(src) if not startwith http://
                ... Add src to filelist
        Urls, Files = FurtherProcessHtml(url, html, Urls, Dloads)
        For each url:
            ShouldVisit(url)?
                ProcessLink(url)
                ... Add url to urlQ
        For each file:
            ShouldDownload(file)?
                ProcessFile(file)
                ... Add file to fileQ



Mercudi.RulesInterface analysis:
================================================================================
StartUrls = ["http://python.org"]  # The url to start crawling with.
If LoadPrevious is true it won't be crawled first. The first non visited
one in the found-links will be visited first.

nNetThreads = 1         # No of Reading thread to spawn
nParseThreads = 1       # No of Parsing thread to spawn
nDownloadThreads = 1    # No of Downloading thread to spawn

TIPS: Set Reader and Parser threads no to zero to download previously the listed files.
TIPS: Set Downloader threads no to zero to crawl the site first and then download.

MinFileSize = 25 * 1000   # means 25KB minimum download size
MaxFileSize = 300 * 1000   # 300KB maximum

MaxSleepTime = 5    # Reader and Downloader will wait for some time
MinSleepTime = 3    # in between before making its next request to the server.

UrlQSize = 10000    # if there are too many urls in the Q the new one won't be added
NOTE: The newly found url will NOT be saved to the state either!


About Saving State
========================================================================
Whenever a new link is added to urlQ it is also written in a found-links file.
Whenever a new file is added to fileQ it is also written in a found-files file.
Whenever a url is visited it is also written in a visited-links file.
The visited url contains both links and files.

If LoadPrevState is commanded, Mercudi will try to read these files and add
links and files to the corresponding queues which were not visited before.
So, if you delete only the visited-links file after a crawling session, next time
Mercudi will visit and download all the urls again, this time the urls are there
without parsing.

If the previous state is empty, it starts with the StartUrls

Stopping Mercudi
============================================================================
Mercudi threads run on while loop. After each loop it check if a certain file
exists in the CWD. Hence,

killall.txt         -       will stop all threads
killreader.txt      -       will stop all reader threads
killparser.txt      -       will stop all parser threads
killdownloader.txt      -   will stop all downloader threads

So, you can easily create or copy a txt file in CWD and stop the processes.

Additional info
=============================================================================
There are some helper functions in Mercudi.Help

getBase(url, with_subdomain=bool)
    returns the base of a url.

getTopSubdomain(url)
    returns the top most subdomain of a url.


