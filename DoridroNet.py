import os
import sys
sys.path.append(os.getcwd())

from Mercudi import Mercudi3
from Mercudi.RulesInterface import IRules
import Mercudi.Global as Gl

class Rules(IRules):
    StartUrls = ["http://m.doridro.net/banglasongs/Robindro%20N%20Nazrul%20Songeet/Nazrul%20Shongeet"]
    nNetThreads = nParseThreads = 5
    nDownloadThreads = 0    # LoadPrevState!!
    CrawlerDirectory = os.path.join(os.getcwd(), "DoridroNazruls")
    DownloadsDirectory = os.path.join(CrawlerDirectory, "Downloads")

    def __init__(self):
        super(IRules, self).__init__()
        # for i in range(2,7): self.StartUrls.append("http://sumirbd.mobi/categorylist/2/default/%d.html" %i)

    def ShouldVisit(self, url):
        # print url
        if not url.startswith("http://m.doridro.net/banglasongs/Robindro N Nazrul Songeet/Nazrul Shongeet/"):
            return False

        return True

    def isDownloadableUrl(self, url):
        if url.endswith(".mp3"):
            return True
        return False

    def ShouldDownload(self, url):
        if url.startswith("http://dl.doridro.net/get/"):
            return True
        return False

    def BrowserUserAgent(self):
        return Gl.Headers["android-chrome"]

Mercudi3.Run(Rules, LoadPrevState = True)
# Mercudi3.Run(Rules, LoadPrevState = False)
