import os
import sys
sys.path.append(os.getcwd())

from Mercudi import Mercudi3
from Mercudi.RulesInterface import IRules
import Mercudi.Global as Gl

class Rules(IRules):
    StartUrls = ["http://m.sumirbd.mobi/categorylist/1077/default/1.html"]
    nNetThreads = nParseThreads = 4
    nDownloadThreads = 0    # LoadPrevState!!
    CrawlerDirectory = os.path.join(os.getcwd(), "SumirbdGazals")
    DownloadsDirectory = os.path.join(CrawlerDirectory, "Downloads")

    def __init__(self):
        super(IRules, self).__init__()
        # for i in range(2,7): self.StartUrls.append("http://sumirbd.mobi/categorylist/2/default/%d.html" %i)

    def ShouldVisit(self, url):
        if not url.startswith("http://sumirbd.mobi/"):
            return False

        if "a2z" in url: return False
        if url == "http://sumirbd.mobi/": return False
        if url.startswith("http://sumirbd.mobi/categorylist/1/"): return False
        if url.startswith("http://sumirbd.mobi/categorylist/96/"): return False
        if url.startswith("http://sumirbd.mobi/categorylist/1029/"): return False
        if url.startswith("http://sumirbd.mobi/categorylist/1037/"): return False

        if "fileList" in url:
            if "new2old" in url: return True
        if "fileDownload" in url: return True
        if "categorylist" in url: return True

        return False

    def isDownloadableUrl(self, url):
        if url.startswith("http://sumirbd.mobi/"):
            if "files/download/id" in url:
                return True
        return False

    def ShouldDownload(self, url):
        if url.startswith("http://sumirbd.mobi/"):
            if "files/download/id" in url:
                return True
        return False

Mercudi3.Run(Rules, LoadPrevState = True)
#Mercudi3.Run(Rules, LoadPrevState = False)
