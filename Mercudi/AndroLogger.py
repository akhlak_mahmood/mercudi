__author__ = 'Akhlak Mahmood'

# A special android logger for Py4Droid

import android
droid = android.Android()

class LogCat:

  def __init__(self, tag=None):
    self.tag = "py4d"
    if tag is not None: self.tag = str(tag)

  def v(self, tag, msg=None):
    if msg is None:
        msg = tag
        tag = self.tag
    droid.logV(tag, msg)

  def d(self, tag, msg=None):
    if msg is None:
        msg = tag
        tag = self.tag
    droid.logD(tag, msg)

  def w(self, tag, msg=None):
    if msg is None:
        msg = tag
        tag = self.tag
    droid.logW(tag, msg)

  def i(self, tag, msg=None):
    if msg is None:
        msg = tag
        tag = self.tag
    droid.logI(tag, msg)

  def e(self, tag, msg=None):
    if msg is None:
        msg = tag
        tag = self.tag
    droid.logE(tag, msg)

  def V(self, tag, msg=None):
    if msg is None:
        msg = tag
        tag = self.tag
    droid.logV(tag, msg)

  def D(self, tag, msg=None):
    if msg is None:
        msg = tag
        tag = self.tag
    droid.logD(tag, msg)

  def W(self, tag, msg=None):
    if msg is None:
        msg = tag
        tag = self.tag
    droid.logW(tag, msg)

  def I(self, tag, msg=None):
    if msg is None:
        msg = tag
        tag = self.tag
    droid.logI(tag, msg)

  def E(self, tag, msg=None):
    if msg is None:
        msg = tag
        tag = self.tag
    droid.logE(tag, msg)

  def wtf(self, tag, msg=None):
    if msg is None:
        msg = tag
        tag = self.tag
    droid.logWTF(tag, msg)
