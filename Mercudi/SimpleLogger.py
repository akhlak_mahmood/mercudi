__author__ = 'Akhlak Mahmood'

import logging
import os

###BEG Logger init
Log = logging.getLogger('mercudi')
Log.setLevel(logging.DEBUG)
fh = logging.FileHandler(os.path.join(os.getcwd(), 'Mercudi3.log'), mode='w+')
fh.setLevel(logging.DEBUG)
fh.setFormatter(logging.Formatter('%(levelname)s - %(message)s'))
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
ch.setFormatter(logging.Formatter('%(levelname)s >> %(message)s'))
Log.addHandler(fh)
Log.addHandler(ch)
del fh, ch
###END Logger init


class MySimpleLogger:
  def __init__(self, tag=None):
    self.tag = "mercudi"
    if tag is not None: self.tag = str(tag)

  def v(self, tag, msg=None):
    if msg is None:
        msg = tag
        tag = self.tag
    Log.debug(msg)

  def d(self, tag, msg=None):
    if msg is None:
        msg = tag
        tag = self.tag
    Log.debug(msg)

  def w(self, tag, msg=None):
    if msg is None:
        msg = tag
        tag = self.tag
    Log.warn(msg)

  def i(self, tag, msg=None):
    if msg is None:
        msg = tag
        tag = self.tag
    Log.info(msg)

  def e(self, tag, msg=None):
    if msg is None:
        msg = tag
        tag = self.tag
    Log.error(msg)

  def V(self, tag, msg=None):
    if msg is None:
        msg = tag
        tag = self.tag
    Log.debug(msg)

  def D(self, tag, msg=None):
    if msg is None:
        msg = tag
        tag = self.tag
    Log.debug(msg)

  def W(self, tag, msg=None):
    if msg is None:
        msg = tag
        tag = self.tag
    Log.warn(msg)

  def I(self, tag, msg=None):
    if msg is None:
        msg = tag
        tag = self.tag
    Log.info(msg)

  def E(self, tag, msg=None):
    if msg is None:
        msg = tag
        tag = self.tag
    Log.error(msg)

  def wtf(self, tag, msg=None):
    if msg is None:
        msg = tag
        tag = self.tag
    Log.fatal(msg)

