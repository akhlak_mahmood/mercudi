# Global module for sharing accross the classes
# Contains only constants

import os

# Needed regex to search for links and images.
Regex = {
      'anchor': r'<a .*?href *= *["\'](.*?)["\'].*?>',
      'anchor-name': r'<a .*?href *= *["\'](.*?)["\'].*?>(.*?)</ *a *>',
      'jpglink': r'<a .*?href *= *["\'](.*?.jpg)["\'].*?>',
      'frame': r'<frame .*?src *= *["\'](.*?)["\'].*?>',
      'iframe': r'<iframe .*?src *= *["\'](.*?)["\'].*?>',
      'img': r'<img .*?src *= *["\'](.*?)["\'].*?>',
      'googleimg': r'imgurl=(http://.*?)&',
    }

Headers = {
    "windows": 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36',
    "android-chrome-desktop": 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.45 Safari/535.19',
    "android-chrome": 'Mozilla/5.0 (Linux; Android 4.1.1; Galaxy Nexus Build/JRO03C) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166 Mobile Safari/535.19',
    "google-bot": "Googlebot/2.1 (+http://www.googlebot.com/bot.html)",
    "bing-bot": "Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)",
    "android-firefox": "Mozilla/5.0 (Android; Mobile; rv:14.0) Gecko/14.0 Firefox/14.0",
    "windows-phone": "Mozilla/5.0 (compatible; MSIE 10.0; Windows Phone 8.0; Trident/6.0; IEMobile/10.0; ARM; Touch; NOKIA; Lumia 920)",
    "iphone": "Mozilla/5.0 (iPhone; CPU iPhone OS 5_0 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9A334 Safari/7534.48.3",
    "android": 'Mozilla/5.0 (Linux; U; Android 2.2; en-us; Nexus One Build/FRF91) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1',
}

Ignore_exts = (".js", ".css", ".xml")
Download_exts = (".jpg", ".gif", ".png")

# Queues for threads
Q_url = None
Q_html = None
Q_file = None

# Browser
Browser = None
# Global Logger
Log = None
# To track threads running condition
ThreadPool = None

#No of downloadables in the queue
nFoundFiles = 0

# Visited links
Visited = []

# Found link
FoundLinks = []

# Found file
FoundFiles = []

Rule = None

NL = "\n"

VisitedFile = os.path.join(os.getcwd(), "visited-links.txt")   # Common for both Reader and Downloader
LinksFile = os.path.join(os.getcwd(), "found-links.txt")
DownloadsFile = os.path.join(os.getcwd(), "found-files.txt")
