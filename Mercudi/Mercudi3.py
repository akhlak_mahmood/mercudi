#!/usr/bin/env python
""" Mercudi3.py
A Mercudi3 crawler.
Dependency: Python 2.6 or 2.7, Mechanize

SEE README.TXT FOR DETAILED INSTRUCTIONS
"""

__author__      = "Akhlak Mahmood"
__version__     = "0.3.3"
__date__        = "Jan 1, 2015"

import os
import Queue
import threading
import cookielib
import time
import warnings

try:
    import Mechanize
except ImportError:
    print "ImportError: Mercudi3 depends on Mechanize!"
    raise

# import the configuration file and helper functions.
import Help
import Request
import Parser
import Downloader
import RulesInterface
import Global as Gl

### BEG:: Initializing Globals

# LogCat for Android
try:
    import android
    import AndroLogger
    Gl.Log = AndroLogger.LogCat("mercudi")
except ImportError:
    from SimpleLogger import MySimpleLogger
    Gl.Log = MySimpleLogger("Mercudi3")

# init Thread Pool
Gl.ThreadPool = threading.Event()

# Queues for threads
Gl.Q_url = Queue.Queue()
Gl.Q_html = Queue.Queue()
Gl.Q_file = Queue.Queue()

### END:: Initializing Globals

def _init_browser():
    ###BEG: Init Mechanize
    Gl.Browser = Mechanize.Browser()
    Gl.Browser.set_cookiejar(cookielib.LWPCookieJar())
    Gl.Browser.set_handle_equiv(True)
    Gl.Browser.set_handle_redirect(True)
    Gl.Browser.set_handle_referer(True)
    # gzip support is experimental, will raise a warning if turned on
    # So ignore all warnings
    warnings.simplefilter("ignore")
    Gl.Browser.set_handle_gzip(True)
    Gl.Browser.set_handle_robots(False)
    Gl.Browser.set_handle_refresh( Mechanize._http.HTTPRefreshProcessor (), max_time=2)
    # Headers
    Gl.Browser.addheaders = [('User-agent', Gl.Rule.BrowserUserAgent()),
                          ('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'),
                          ('Accept-Encoding', 'gzip,deflate'),
                          ('Accept-Language', 'en-US,en;q=0.8'),
                          ('Accept-Charset', 'ISO-8859-1,utf-8;q=0.7,*;q=0.3')]
    ###END: Init Mechanize

def _init_rules(rule_class):
    Gl.Log.d("Initializing Rules ...")
    if issubclass(rule_class, RulesInterface.IRules):
        Gl.Rule = rule_class()

        if isinstance(Gl.Rule.StartUrls, str):
            Gl.Rule.StartUrls = [Gl.Rule.StartUrls]

        # Can not but do it here :/
        Gl.VisitedFile = os.path.join(Gl.Rule.CrawlerDirectory, "visited-links.txt")
        Gl.LinksFile = os.path.join(Gl.Rule.CrawlerDirectory, "found-links.txt")
        Gl.DownloadsFile = os.path.join(Gl.Rule.CrawlerDirectory, "found-files.txt")

        Gl.Log.d("Rules initialized")
        return True
    else:
        Gl.Log.wtf("Rules class not valid! It must be a subclass of "
                   "Mercudi.RulesInterface.IRules. Please see README.TXT "
                   "for more details.")
        return False

def _remove_state():
    Gl.Log.d("Removing previous state...")

    try:
        if os.path.exists(Gl.VisitedFile):
            os.remove(Gl.VisitedFile)
    except:
        Gl.Log.w("Delete Error: %s" %Gl.VisitedFile)

    try:
        if os.path.exists(Gl.LinksFile):
            os.remove(Gl.LinksFile)
    except:
        Gl.Log.w("Delete Error: %s" %Gl.LinksFile)

    try:
        if os.path.exists(Gl.DownloadsFile):
            os.remove(Gl.DownloadsFile)
    except:
        Gl.Log.w("Delete Error: %s" %Gl.DownloadsFile)

    Gl.Log.d("Done: Removing previous state")


def _load_previous_state():
    Gl.Log.d("Loading previous items...")

    Gl.Visited = Help.GetFileConfig(Gl.VisitedFile, False)

    for link in Help.GetFileConfig(Gl.LinksFile, False):
        if (not link in Gl.Visited) and (not link in Gl.FoundLinks):
            Gl.Q_url.put(link)
            Gl.FoundLinks.append(link)

    for link in Help.GetFileConfig(Gl.DownloadsFile, False):
        if (not link in Gl.Visited) and (not link in Gl.FoundFiles):
            Gl.Q_file.put(link)
            Gl.FoundFiles.append(link)
            Gl.nFoundFiles += 1

    # if previous state was empty, load start url
    if Gl.Q_url.qsize() == 0 and Gl.Q_file.qsize() == 0:
        for su in Gl.Rule.StartUrls:
            Gl.Q_url.put(su)
        Gl.Log.w("Previous state is empty!")
    else:
        Gl.Log.i("Total loaded links: %d, files: %d" %(len(Gl.FoundLinks), len(Gl.FoundFiles)))

    Gl.Log.i("Done: Loading previous items")

def Run(rule_class, LoadPrevState = True):
    Gl.ThreadPool.set()

    if os.path.exists('killall.txt'):
        os.remove('killall.txt')
    if os.path.exists('killreader.txt'):
        os.remove('killreader.txt')
    if os.path.exists('killparser.txt'):
        os.remove('killparser.txt')
    if os.path.exists('killdownloader.txt'):
        os.remove('killdownloader.txt')

    # init Rule
    if not _init_rules(rule_class):
        return

    # init browser
    _init_browser()

    # create crawler directory
    if not os.path.exists(Gl.Rule.CrawlerDirectory):
        os.makedirs(Gl.Rule.CrawlerDirectory)
        Gl.Log.d("Created directory: %s" %Gl.Rule.CrawlerDirectory)

    # create downloads directory, by default inside CrawlerDirectory
    if not os.path.exists(Gl.Rule.DownloadsDirectory):
        os.makedirs(Gl.Rule.DownloadsDirectory)
        Gl.Log.d("Created directory: %s" %Gl.Rule.DownloadsDirectory)

    if LoadPrevState:
        _load_previous_state()
    else:
        _remove_state()
        if Gl.Q_url.empty():
            for su in Gl.Rule.StartUrls:
                Gl.Q_url.put(su)
                Gl.FoundLinks.append(su)

    Gl.Log.d("Starting Threads...")

    for i in range(Gl.Rule.nNetThreads):
        t = Request.UrlReader(i + 1, Gl.Q_url, Gl.Q_html, Gl.Q_file)
        t.start()

    for i in range(Gl.Rule.nParseThreads):
        t = Parser.HtmlProcessor(i + 1, Gl.Q_url, Gl.Q_html, Gl.Q_file)
        t.start()

    for i in range(Gl.Rule.nDownloadThreads):
        t = Downloader.FileDownloader(i + 1, Gl.Q_url, Gl.Q_html, Gl.Q_file)
        t.start()

    nMyThreads = Gl.Rule.nNetThreads + Gl.Rule.nParseThreads + Gl.Rule.nDownloadThreads
    Gl.Log.i("All %d threads started." %nMyThreads)

    try:
        while True:
            time.sleep(Gl.Rule.MinSleepTime)
            if os.path.exists("killall.txt"):
                Gl.Log.i("FOUND: [killall]")
                break

    except KeyboardInterrupt:
        # Break the threads running condition by clearing thread event.
        Gl.Log.i("[KeyboardInterrupt]")

    Gl.ThreadPool.clear()
    Gl.Log.i("Found links: %d, Files: %d, Visited: %d" %(len(Gl.FoundLinks), len(Gl.FoundFiles), len(Gl.Visited)))
    Gl.Log.i("All threads will be closed, Goodbye!")

if __name__ == "__main__":
    print "Sorry, you must define a site specific rule class, import Mercudi3" \
          " and call Mercudi3.Run(rule_class, LoadPrevState) method."
    print "For detailed instructions see README.TXT"

