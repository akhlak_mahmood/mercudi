import os
import threading
import time
import re
from urlparse import *

import Help
import Global as Gl

# get html from htmlQ, parse it and put found links and
# images on the urlQ and fileQ
class HtmlProcessor(threading.Thread):
    def __init__(self, idx, urlq, htmlq, fileq):
        threading.Thread.__init__(self)
        self.fileQ = fileq
        self.urlQ = urlq
        self.htmlQ = htmlq
        self.id = idx

    def run(self):
        sleepTime = 0
        Gl.Log.i("Parser [%d] Started..." %self.id)

        while Gl.ThreadPool.is_set():
            if os.path.exists("killparser.txt"):
                Gl.Log.i("FOUND: [killparser]")
                break
            try:
                try:
                    url, html = self.htmlQ.get(timeout = sleepTime + 2 * Gl.Rule.nParseThreads - Gl.Rule.nNetThreads + Gl.Rule.MaxSleepTime)
                    if sleepTime > 7: sleepTime = 3
                except:
                    sleepTime += 1
                    Gl.Log.d("Parser [%d]: waited for new html." %self.id)
                    continue

                # Parse html
                Gl.Log.i('Parsing [%d]: %s' %(self.id, url))
                try: urls = self.mineTheLinks(html, url)
                except Exception, e:
                    Gl.Log.e("Parser [%d] Mining: %s @ %s" %(self.id , str(e), url))
                    continue

                links = urls['Urls']
                files = urls['Files']

                items = 0

                for anchor in links:
                    if anchor not in Gl.Visited and anchor not in Gl.FoundLinks:
                        # ask Ruler if should add to visiting Q
                        if Gl.Rule.ShouldVisit(anchor):
                            Gl.Log.d("True: %s" %anchor)
                            items += 1
                            anchor = Gl.Rule.ProcessLink(anchor)
                            if self.urlQ.qsize() < Gl.Rule.UrlQSize:
                                self.urlQ.put(anchor)
                                # Note: the link won't be added to state file if Qsize is maximum
                                Help.addLink(anchor)
                            else:
                                Gl.Log.w("URL QUEUE CONTAINS MAXIMUM NUMBER OF URLS, IGNORING NEW URLS.")
                        else: Gl.Log.d("False: %s" %anchor)


                for f in files:
                    if f not in Gl.Visited and f not in Gl.FoundFiles:
                        # ask Ruler if should add to Downloading Q
                        if Gl.Rule.ShouldDownload(f):
                            Gl.Log.d("True: %s" %f)
                            items += 1
                            f = Gl.Rule.ProcessFile(f)
                            self.fileQ.put(f)
                            Help.addFile(f)
                            Gl.nFoundFiles += 1
                        else: Gl.Log.d("False: %s" %f)

                Gl.Log.i("Done parse [%d], found links: %d" %(self.id, items))
                self.htmlQ.task_done()
            except Exception, e:
                Gl.Log.e("Parser [%d] Exception: %s" %(self.id, str(e)))

        Gl.Log.i("Parser [%d] Terminated..." %self.id)
        return

    def getAnchors(self, html, url):
        hrefs = []
        regex = Gl.Rule.AnchorRegex(url)
        if regex is not None:
            # if regex is a list, mine for each one, v033
            if isinstance(regex, list):
                for reg in regex:
                    hrefs += re.findall(reg, html, re.I|re.M)
            else:
                hrefs = re.findall(regex, html, re.I|re.M)

        return hrefs

    def getImgSources(self, html, url):
        hrefs = []
        regex = Gl.Rule.SrcRegex(url)
        if regex is not None:
            # if regex is a list, mine for each one, v033
            if isinstance(regex, list):
                for reg in regex:
                    hrefs += re.findall(reg, html, re.I|re.M)
            else:
                hrefs = re.findall(regex, html, re.I|re.M)

        return hrefs

    def mineTheLinks(self, html, url):
        """
        Retrive all the anchors and image, iframe sources from the html.
        """
        t1 = time.time()
        Dloads = []
        Urls = []

        links = self.getAnchors(html, url)               # get the anchors

        for link in links:
            temp = str(link)
            # depending the regex given the link might be extracted from the result
            link = Gl.Rule.ProcessAnchorRegexResult(url, link)
            if link is None:
                Gl.Log.d("False: %s" %temp)
                continue
            if not link.startswith(("http://", "https://")):
                link = urljoin(url, link)           # v033

            if Gl.Rule.isValidUrl(link):           # check validity
                link = Gl.Rule.FormatUrl(link)     # small formatting

                if Gl.Rule.isDownloadableUrl(link):
                    Dloads.append(link)
                else:
                    Urls.append(link)

        # List normally displayed images...
        if Gl.Rule.DownloadImgSrc():
            imgs = self.getImgSources(html, url)     # get the image srcs
            for src in imgs:
                temp = str(src)
                # depending the regex given the src might be extracted from the result
                src = Gl.Rule.ProcessSrcRegexResult(url, src)
                if src is None:
                    Gl.Log.d("False: %s" %temp)
                    continue
                if not src.startswith(("http://", "https://")):
                    src = urljoin(url, src)         # v033
                Dloads.append(src)

        t2 = time.time()
        Gl.Log.d('Process time: %f' %(t2-t1))
        return Gl.Rule.FurtherProcessHtml(url, html, Urls, Dloads)
