### THIS IS A MODULE REQUIRED FOR MERCUDI3
### SEE README.TXT FOR ANY HELP YOU NEED


import os
import time
import random
import re
import gzip
from urlparse import *

import Mechanize
import Global as Gl

##BEG Helpers Functions/Classes

class HeadRequest(Mechanize.Request):
    """ A class to make http HEAD request. """

    def get_method(self):
        return "HEAD"


# Continuous http request may raise alarm to the server
# and may harm in rare case.
def takeRest():
    """ Sleep for a random time between the max min range """
    sec = random.uniform(Gl.Rule.MinSleepTime, Gl.Rule.MaxSleepTime)
    time.sleep(sec)
    return True

def unique(s):
    """ Accepts a list and returns it with only unique items.
    Found at: http://aspn.activestate.com/ASPN/Cookbook/Python/Recipe/52560
    """
    n = len(s)
    if not n:
        return []
    u = {}
    try:
        for x in s: u[x] = 1
    except TypeError:
        del u
    else:
        return u.keys()
    try:
        t = list(s)
        t.sort()
    except TypeError:
        pass
    else:
        assert n > 0
        last = t[0]
        lasti = i = 1
        while i < n:
            if t[i] != last:
                t[lasti] = last = t[i]
                lasti += 1
            i += 1
        return t[:lasti]
    u = []
    for x in s:
        if x not in u: u.append(x)
    return u


def GetFileConfig(filename, isConfig=True):
    """ Reads the configuration file specified in the parameter
    and returns a list containing the terms in it.
    If the specified file does not exists, its created."""

    default_config = \
        """# Mercudi config list.
        # All lines must be VALID regex and thus properly escaped.
        # Lines starting with hash(#) are comments.
        # Empty lines will be ignored.

        """

    if os.path.exists(filename):
        configfile = open(filename, "r")
    else:
        try:
            f = open(filename, 'w')
            if isConfig:
                f.write(default_config)
            f.close()
            configfile = open(filename, "r")
        except IOError:
            return False
    ret = []
    for line in configfile:
        line = line.strip()
        if not line or line[0] == '#':
            continue
        else:
            line = r'%s' % line
            ret.append(line)
    configfile.close()
    return unique(ret)

def getTopSubdomain(url):
    """ Returns top most subdomain if exists. Else None. """
    if not url.startswith("http://"):
        url = "http://" + url
    base = urlparse(url).netloc
    if base.count(".") > 1:
        return base.split(".")[0]
    else:
        return None

def getBase(url, subdomain = False):
    """ Returns base url, including subdomain if subdomain is True with a trailing slash. """
    if not url.startswith("http://"):
        url = "http://" + url
    if subdomain:
        return urlparse(url).netloc + "/"
    if url.count('/') > 2:
        url = re.findall(r'http://.*?/', url, re.I)[0]
    else:
        url += u'/'
    return '.'.join(url.split('.')[-2:])


def VisitedAppend(url):
    """ Adds a url to the visited list and append to the visited file. """
    if not url in Gl.Visited:
        Gl.Visited.append(url)
        try:
            f = open(Gl.VisitedFile, 'a+')
            f.write(url + Gl.NL)
            f.close()
        except Exception, e:
            Gl.Log.e("Failed to append to %s" % Gl.VisitedFile)


def addLink(url):
    Gl.FoundLinks.append(url)
    if not url in Gl.Visited:
        try:
            f = open(Gl.LinksFile, 'a+')
            f.write(url + Gl.NL)
            f.close()
        except Exception, e:
            Gl.Log.e("Failed to append to %s" % Gl.LinksFile)

def addFile(url):
    Gl.FoundFiles.append(url)
    if not url in Gl.Visited:
        try:
            f = open(Gl.DownloadsFile, 'a+')
            f.write(url + Gl.NL)
            f.close()
        except Exception, e:
            Gl.Log.e("Failed to append to %s" % Gl.DownloadsFile)


def ungzip(r, b):
    """ Unzips a gzipped html and set it to the browser's response.
    Browser is supplied via parameter. """
    headers = r.info()
    if ('Content-Encoding' in headers.keys() and headers['Content-Encoding'] == 'gzip') or \
            ('content-encoding' in headers.keys() and headers['content-encoding'] == 'gzip'):
        gz = gzip.GzipFile(fileobj=r, mode='rb')
        try:
            html = gz.read()
        except IOError:
            return
        gz.close()
        headers['Content-type'] = 'text/html; charset=utf-8'
        r.set_data(html)
        b.set_response(r)

##END Helper Functions
