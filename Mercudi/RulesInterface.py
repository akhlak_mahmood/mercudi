__author__ = 'Akhlak Mahmood'

import os
from urlparse import *

import Global as Gl

class IRules(object):
    """
    [Call Hierarchy]
    For each html in the htmlQ:
        ... extract link from html using given regex
        For each item:
            ProcessAchorRegexResult(item)
            urljoin(item) if not startwith http://
            isValidUrl(item)?
                FormatUrl(item)
                isDownloadableUrl(item)?
                    ... Add item to Dloads
                else:
                    ... Add item to Urls
        DownloadImgSrc?
            ... Get all img srcs from the html using the given regex
            For each src:
                ProcessSrcRegexResult(src)
                urljoin(src) if not startwith http://
                ... Add src to filelist
        Urls, Files = FurtherProcessHtml(url, html, Urls, Dloads)
        For each url:
            ShouldVisit(url)?
                ProcessLink(url)
                ... Add url to urlQ
        For each file:
            ShouldDownload(file)?
                ProcessFile(file)
                ... Add file to fileQ
    """
    StartUrls = ["http://python.org"]

    nNetThreads = 1
    nParseThreads = 1
    nDownloadThreads = 1

    MinFileSize = 25 * 1000   # in bytes
    MaxFileSize = 300 * 1000   # in bytes

    MaxSleepTime = 5
    MinSleepTime = 3

    UrlQSize = 10000

    CrawlerDirectory = os.getcwd()
    DownloadsDirectory = os.path.join(CrawlerDirectory, "Downloads")

    def __init__(self):
        pass

    def ShouldVisit(self, url):
        """
        The most important method: should this url be visited?
        Return True or False

        This is the final check before adding to the urlQ
        Called by: Parser
        """
        return True

    def ShouldDownload(self, url):
        """
        The second most important method: should this url be downloaded?
        Return True or False

        This is the final check before adding to the downloadQ
        Called by: Parser, Reader
        """
        return False

    def DownloadImgSrc(self):
        """
        The third most important method: should the static images
        displayed on the page as <img src=""/> also be downloaded?
        Called by: Parser
        """
        return False

    def isValidUrl(self, url):
        """
        Is this url OK to visit? Not javascript or anchor?
        Note: this check is done for anchors and iframe srcs and also
        for the links to potential downloadable files.
        Not done for static img src in case ShouldAddImg(self) is True.
        Called by: Parser
        """
        if url.startswith(("javascript:", "mailto:")):
            return False
        url = url.lower()
        if url.endswith(Gl.Ignore_exts): return False
        try:
            u = urlparse(url)
        except:
            return False
        else:
            if u.path.endswith(Gl.Ignore_exts): return False
        return True

    def FormatUrl(self, url):
        """
        Remove unnecessary part, if any, e.g. #anchor and return the good part.
        Otherwise just return it back.
        Not done for static img src in case ShouldAddImg(self) is True.
        Called by: Parser
        """
        return url.split("#")[0]

    def isDownloadableUrl(self, url):
        """
        Is this url a file to be downloaded?
        Called by: Parser
        """
        url = url.lower()
        if url.endswith('.jpg'): return True
        if url.endswith(Gl.Download_exts): return True
        try:
            u = urlparse(url)
        except:
            return False
        else:
            if u.path.endswith(Gl.Download_exts): return True
            if u.query:
                for q in u.query.split("&"):
                    if q.endswith(Gl.Download_exts): return True

        return False

    def ProcessLink(self, url):
        """
        The url should be visited, ok fine, now do you want to
        modify it? You know, extract the query etc?
        Just return back the best part to visit.

        Otherwise, never mind, just give the url back.

        Called only if ShouldVisit() is True
        Called just before adding to the Q.
        Called by: Parser
        """
        return url

    def ProcessFile(self, url):
        """
        The url should be downloaded, ok fine, now do you want to
        modify it into something? You know, extract the query part, or etc.?
        Return back the best part to download.

        Otherwise, just give the url back.

        Called only if ShouldDownload() is True
        Called just before adding to the Q.
        Called by: Parser
        """
        return url

    def AnchorRegex(self, url):
        """
        Return the regex or list of regex to search for <a href=""/>

        You can add aditional tags to mine, say iframe by giving a
        regex for it. Each result will be sent back to the
        ProcessAnchorRegexResult(url, result) so that you can check
        each of them.

        Return None to skip the search
        Called by: Parser
        """
        return Gl.Regex['anchor']

    def ProcessAnchorRegexResult(self, url, result):
        """
        Return the href from the given regex result
        You may return None to disregard the current link

        This is called with each result of regular expression
        search done by the given regex. A url can be Falsed
        by returning none.
        Note: Result contains raw url. urljoin() is not done.

        Return string/None
        Called by: Parser
        """
        if isinstance(result, str):
            return result
        else: return result[0]

    def SrcRegex(self, url):
        """
        Return the regex or list of regex to search for <img src=""/>
        Return None to skip the search
        Similar to AnchorRegex()
        Called by: Parser
        """
        return Gl.Regex['img']

    def ProcessSrcRegexResult(self, url, result):
        """
        Return the src from the given regex result
        You may return None to disregard the current src

        This is called with each result of regular expression
        search done by the given regex. A url can be Falsed
        by returning none.
        Note: Result contains raw url. urljoin() is not done.

        Return string/None
        Called by: Parser
        """
        if isinstance(result, str):
            return result
        else: return result[0]

    def FurtherProcessHtml(self, url, html, foundlinks, foundfiles):
        """
        I have done what I could, now I am giving you the url, it's html,
        the links and files I found. Now, you can mine the html yourself
        if you want, say, using BeautifulSoup. Or perhaps extract some
        other things? Do what ever you want.

        Just return the list of valid urls and files as
        a dictionary with keys "Urls" and "Files".

        If nothing is needed, just return the links and files I found.
        Called by: Parser
        """
        return {'Urls': foundlinks, 'Files': foundfiles}

    def BrowserUserAgent(self):
        """
        Return a user-agent string for the browser.
        You can use one already defined in the Globals.Headers
        Called by: Mercudi
        """
        return Gl.Headers["windows"]
