import os
import threading
import time

try:
    import Mechanize
except ImportError:
    print "ImportError: Mercudi3 depends on Mechanize!"
    raise

# import the configuration file and helper functions.
import Help
import Global as Gl

# get file url from fileQ and download it.
class FileDownloader(threading.Thread):
    def __init__(self, idx, urlq, htmlq, fileq):
        threading.Thread.__init__(self)
        self.urlQ = urlq
        self.htmlQ = htmlq
        self.fileQ = fileq
        self.id = idx

    def run(self):
        sleepTime = 0
        Gl.Log.i("Downloader [%d] Started..." %self.id)
        while Gl.ThreadPool.is_set():
            if os.path.exists("killdownloader.txt"):
                Gl.Log.i("FOUND: [killdownloader]")
                break

            try:
                try:
                    file_url = self.fileQ.get(timeout = sleepTime + 2 * Gl.Rule.nDownloadThreads - Gl.Rule.nParseThreads)
                    if sleepTime > 7: sleepTime = 3
                except:
                    sleepTime += 1
                    Gl.Log.d("Downloader [%d]: waited for new file." %self.id)
                    continue
                if not file_url in Gl.Visited:
                    # guess file and server name from the url
                    name = file_url.split('/')[-1].split('?')[0]
                    server = file_url.split('/')[2]
                    Gl.Log.i('Downloading [%d]: %s (%s)' %(self.id, name, server))
                    try: file_response = Gl.Browser.open(Help.HeadRequest(file_url))
                    except BaseException, e:
                        Gl.Log.w('Download [%d] HeadRequest Failed: %s' %(self.id, file_url))
                        Gl.Log.e(str(e))

                        # Download it directly...
                        Gl.Log.w("Downloading [%d] directly: %s" %(self.id, file_url))
                        fname = u"%s-%s-%s" % (server, ('%.2f' % time.time()).replace('.', '_'), name)
                        try:
                            Gl.Browser.retrieve(file_url, os.path.join(Gl.Rule.DownloadsDirectory, fname))
                        except Exception, e:
                            Gl.Log.w('Failed [%d] download: %s!' %(self.id, name))
                            Gl.Log.e(str(e))
                        else:
                            Gl.nFoundFiles -= 1
                            Help.VisitedAppend(file_url)
                            Gl.Log.i("Download [%d] OK: %s (size: unknown) Queue: %d" %(self.id, name, Gl.nFoundFiles))
                    else:
                        try:
                            meta = file_response.info()
                            file_response.close()                       # won't need anymore

                            if not "Content-type" in meta:
                                meta["Content-type"] = "unknown"        # Just incase
                        except Exception, e:
                            Gl.Log.e("Meta: %s @ %s" %(str(e), file_url))
                        else:
                            if "text" in meta["Content-type"] or "html" in meta["Content-type"]:
                                # a text file found, fetch the content and add to parsing queue
                                try:
                                    r = Gl.Browser.open(file_url)
                                except Exception, e:
                                    Gl.Log.e("Downloader [%d] UrlOpen: %s @ %s" %(self.id, str(e), file_url))
                                    continue

                                Help.ungzip(r, Gl.Browser) # Experimental, may cause problem with the global Br
                                try: html = r.read()
                                except Exception, e:
                                    Gl.Log.e("Read: %s @ %s" %(str(e), file_url))
                                    continue
                                finally: r.close()

                                if "html" in html.lower():
                                    self.htmlQ.put((file_url, html))
                                    Gl.Log.w("Downloader found: '%s' (sent it to Parser). Url: %s" %(meta["Content-type"], file_url))
                                else:
                                    Gl.Log.w("Downloader found: '%s' but no 'html' was present. Url: %s" %(meta["Content-type"], file_url))
                                Gl.nFoundFiles -= 1
                            else:
                                # Check size and download it.
                                try:
                                    size = int(meta.getheaders('Content-Length')[0])
                                except:
                                    size = 0

                                Gl.Log.d('File: %s Size: %d bytes.' %(name, size))
                                if size < Gl.Rule.MinFileSize:
                                    Gl.Log.w('Too Small: %s Queue: %d' %(name, Gl.nFoundFiles))
                                    Gl.nFoundFiles -= 1
                                    Help.VisitedAppend(file_url)
                                elif size > Gl.Rule.MaxFileSize:
                                    Gl.Log.w('Too Big: %s Queue: %d' %(name, Gl.nFoundFiles))
                                    Gl.nFoundFiles -= 1
                                    Help.VisitedAppend(file_url)
                                else:
                                    # Download it...
                                    fname = u"%s-%s-%s" % (server, ('%.2f' % time.time()).replace('.', '_'), name)
                                    try:
                                        Gl.Browser.retrieve(file_url, os.path.join(Gl.Rule.DownloadsDirectory, fname))
                                    except Exception, e:
                                        Gl.Log.w('Failed download [%d]: %s @ %s' %(self.id, str(e), name))
                                    else:
                                        Gl.nFoundFiles -= 1
                                        Help.VisitedAppend(file_url)
                                        Gl.Log.i("Download [%d] OK: %s (%d b) Queue: %d" %(self.id, name, size, Gl.nFoundFiles))

                self.fileQ.task_done()
                Help.takeRest()
            except Exception, e:
                Gl.Log.e("Downloader [%d] Exception: %s" %(self.id, str(e)))

        Gl.Log.i("Downloader [%d] Terminated..." %self.id)
        return
