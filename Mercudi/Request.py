import os
import threading
import Queue

import Help
import Global as Gl


# get url from urlQ read it and store the html data
# on htmlQ for parsing
class UrlReader(threading.Thread):
    def __init__(self, idx, urlq, htmlq, fileq):
        threading.Thread.__init__(self)
        self.urlQ = urlq
        self.htmlQ = htmlq
        self.fileQ = fileq
        self.id = idx

    def run(self):
        sleepTime = 0 # Waiting timeouts in seconds
        Gl.Log.i("Reader [%d] started..." %self.id)

        while Gl.ThreadPool.is_set():
            if os.path.exists("killreader.txt"):
                Gl.Log.i("FOUND: [killreader]")
                break
            try:
                try:
                    # Get url from Q
                    url = self.urlQ.get(timeout = sleepTime + 2 * Gl.Rule.nNetThreads - Gl.Rule.nParseThreads)
                    if sleepTime > 5: sleepTime = 3
                except:
                    sleepTime += 1 # Increment so next time it'll wait longer
                    Gl.Log.d("Reader [%d]: waited for new url." %self.id)
                    if Gl.Q_url.empty() and Gl.Q_file.empty() and Gl.Q_html.empty():
                        if self.id == 1:      # only 1st Reader should put the url to retry
                            url = Gl.Rule.StartUrls[0]

                            # if start url is already visited
                            # means it is already parsed but now all Qs are empty.
                            # ask to quit manually!
                            if url in Gl.Visited:
                                Gl.Log.w("All queues are empty and StartUrls[0] is already visited! "
                                         "Seems like crawling is done. If so stop it manually by "
                                         "sending [killall]. See README.TXT for details.")
                            else:
                                self.urlQ.put(url)
                    continue

                if not url in Gl.Visited:
                    Gl.Log.i('Reading [%d]: %s' %(self.id, url))
                    # Try to make a http head request first
                    try: r = Gl.Browser.open(Help.HeadRequest(url))
                    except Exception, e:
                        Gl.Log.w('Failed Head Request: %s' %url)
                        Gl.Log.e(str(e))

                        # try to open without headrequest
                        Gl.Log.d("Reading [%d] directly: %s" %(self.id, url))
                        try:
                            r = Gl.Browser.open(url)
                        except Exception, e:
                            Gl.Log.w("UrlOpen Error: %s" %url)
                            Gl.Log.e(str(e))
                            continue
                        else:
                            Help.ungzip(r, Gl.Browser) # Experimental, may cause problem with the global Br

                            try: html = r.read()
                            except Exception, e:
                                Gl.Log.e("Read [%d]: %s @ %s" %(self.id, str(e), url))
                                continue
                            finally: r.close()

                            if "html" in html.lower():
                                self.htmlQ.put((url, html))
                                Gl.Log.i("Read [%d]: %s" %(self.id, url))
                                Help.VisitedAppend(url)
                            else:
                                # most probably not html, so don't send to parser
                                Gl.Log.warn("No html: %s" %url)
                                # ask if to treat as file
                                if Gl.Rule.ShouldDownload(url):
                                    url = Gl.Rule.ProcessFile(url)
                                    self.fileQ.put(url)
                                    Help.addFile(url)
                                    Gl.nFoundFiles += 1
                                else: Help.VisitedAppend(url)
                    else:
                        # Analyze the response headers, if OK, download the file
                        try:
                            meta = r.info()
                            r.close()                       # won't need anymore
                            if not "Content-type" in meta:
                                meta["Content-type"] = "unknown"        # Just incase
                        except Exception, e:
                            Gl.Log.e("Meta: %s @ %s" %(str(e), url))
                            continue

                        if "text" in meta["Content-type"] or "html" in meta["Content-type"]:
                            try: r = Gl.Browser.open(url)
                            except Exception, e:
                                Gl.Log.e("UrlOpen: %s @ %s" %(str(e), url))
                                continue

                            try:
                                Help.ungzip(r, Gl.Browser) # Experimental, may cause problem with the global Br
                            except Exception, e:
                                Gl.Log.e("UnGZip%s @ %s" %(str(e), url))
                                continue

                            try: html = r.read()
                            except Exception, e:
                                Gl.Log.e("Read [%d]: %s @ %s" %(self.id, str(e), url))
                                continue
                            finally: r.close()

                            if "html" in html.lower():
                                self.htmlQ.put((url, html))
                                Gl.Log.i("Read [%d]: %s" %(self.id, url))
                            else:
                                # most probably not html, so don't send to parser
                                Gl.Log.warn("Reader found '%s' but no 'html' was present,"
                                            " file ignored. Url: %s" %(meta["Content-type"], url))
                            Help.VisitedAppend(url)
                        # Not a text file...
                        else:
                            # image found instead, add to downloads queue
                            if "image" in meta["Content-type"] or "jpeg" in meta["Content-type"]:
                                url = Gl.Rule.ProcessFile(url)
                                self.fileQ.put(url)
                                Help.addFile(url)
                                Gl.nFoundFiles += 1
                                Gl.Log.w("Reader [%d] found: '%s' (sent to Downloader). Url: %s" %(self.id, meta["Content-type"], url))
                            # Its not image either, ignore it.
                            else:
                                Gl.Log.e("Non text/image content: '%s' Url: %s" %(meta["Content-type"], url))
                                Help.VisitedAppend(url)

                self.urlQ.task_done()

                # compulsory to take some rest
                Help.takeRest()

                # retry if only given url failed
                if self.urlQ.empty() and len(Gl.Visited) == 0:
                    # only 1st Reader should add url to retry
                    if self.id == 1: self.urlQ.put(Gl.Rule.StartUrls[0])

            except Exception, e:
                Gl.Log.e("Reader [%d] Exception: %s" %(self.id, str(e)))


        # while loop condition broken
        Gl.Log.i("Reader [%d] Terminated..." %self.id)
        return
